package main

import "fmt"

/*
Question: Write a simple program that reverses a string input

Pseudocode:
1. Receive input
2. Loop through string characters decrementing counter
   and adding string to new string variable
3. Return result
*/

func reverseString(str string) string{
	count := len(str) - 1
	var revStr string = ""


	for i := 0; i < len(str); i ++ {
		for count >= 0{
			revStr += string(str[count])
			count--
		}
	}

	return revStr
}

func main()  {
	fmt.Println(reverseString("Sendy"))
}
