package main

import "sort"

/*
Question: Write a simple program that takes in an array of integers and returns an array containing their corresponding ranks.

Pseudocode:
1. Receive input
2. Sort array
3. Store the array elements into a map
4. Unpack map elements into another array
*/

func findRank(nums []int) []int{
	sortedArray := sorter(nums)
	memo := make(map[int]int)
	var ranks []int

	for i := 0; i < len(sortedArray); i++{
		memo[sortedArray[i]] = i
	}

	for k, v := range memo{
		//index of k, v+1
		//nums.indexOf(k)
		ranks = append(ranks, nums[k])
	}

}

func sorter(arr []int) []int{
	sort.Ints(arr)
	return arr
}

func main(){

}