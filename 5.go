package main

import "fmt"

/*
Question: Given two arrays containing tech stacks, write a program that displays the stack(s) contained in both arrays.

Pseudocode:
1. Join the two arrays
2. Store the array elements of the resultant array into a map with their frequencies as the values
3. Return the elements with frequencies equal to 2

Complexity: O(n)
*/

func findIntersection(list1 []string, list2 []string) []string{
	list1 = append(list1, list2...)

	var intersection []string
	memo := make(map[string]int)

	for i, _ := range list1{
		 memo[string(list1[i])] += 1
	}

	for k, v := range memo{
		if v == 2{
			intersection = append(intersection, k)
		}
	}

	return intersection


}

func main(){
	frontend := []string{"CSS", "HTML", "JavaScript", "Vue.js"}
	backend := []string{"Java", "Go", "JavaScript", "PHP"}
 	fmt.Println(findIntersection(frontend, backend))
}
