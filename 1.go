package main

import (
	"fmt"
	"sort"
)

/*

Question: Write a simple program that returns the second largest number from an array

Pseudocode
1. Sort the array
2. Return the [-2] index
*/

func secondLargest(nums []int) []int{
	length := len(nums)
	sort.Ints(nums)
	return nums[length-2:length-1]
}

func main()  {
	numbers := []int{43, 11, 6, 81, 34}
	fmt.Println(secondLargest(numbers))

}
