package main

import "fmt"
import "strings"

/*
Question: Given an array of names, return a new array with the names that start with J

Pseudocode:
1. Take in an array
2. Loop through it and look for strings starting with J
3. Store these strings into another array
4. Output the result

Complexity: O(n)

*/

func startsWithJ(names []string)  []string{
	var returnArray []string
	for _, name := range names{
		if strings.ToLower(string(name[0])) == "j"{
			returnArray = append(returnArray, name)
		}
	}
	return returnArray
}

func main()  {
	names := []string{"Joseph", "Clinton", "Justin", "Mark"}
	fmt.Println(startsWithJ(names))
}
