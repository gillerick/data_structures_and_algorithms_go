package main

import "fmt"

/*
Question: Write a simple algorithm that returns the duplicate number in a list of integers.
Assume that there is always just one repeated integer.

Pseudocode:
1. Loop through the array elements
2. Store their frequencies in a map
3. Return early true since it's only a single duplicate expected

Complexity: O(n)
*/

func findDuplicate(nums []int) int{
	memo := make(map[int]int)
	//map[key] = value

	for _, v := range nums{
		memo[v] += 1
		if memo[v] == 2 {
			return memo[v]
		}

	}
	return 0
}

func main()  {
	numbers := []int{5, 6, 7, 2, 1, 2}
	fmt.Println(findDuplicate(numbers))
}
